package java.agl.tp1;

import java.util.*;



public class Prof {
	public String nom;
	public ArrayList<Creneaux> dispo;
	
	
	public Prof(String nom, ArrayList<Creneaux> dispo) {
		super();
		this.nom = nom;
		this.dispo = dispo;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public ArrayList<Creneaux> getCreneaux() {
		return dispo;
	}
	public void setCreneaux(ArrayList<Creneaux> dispo) {
		this.dispo = dispo;
	}


	
	
}
