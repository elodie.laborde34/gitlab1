package java.agl.tp1;

import java.util.*;

public class Creneaux {
	Calendar cal= Calendar.getInstance();
	
	public Creneaux(Calendar cal) {
		super();
		this.cal = cal;
	}
	
	/* Calendar.set(year + 1900, month, date, hrs, min)
	 * cal.set(Calendar.YEAR, 1988);
	 * cal.set(Calendar.MONTH, Calendar.JANUARY);
	 * cal.set(Calendar.DAY_OF_MONTH, 1);
	 */
	
	public void affichageCreneaux() {
		System.out.print(this.cal.get(Calendar.DATE));
		System.out.print("/");
		System.out.print(this.cal.get(Calendar.MONTH));
		System.out.print("/");
		System.out.print(this.cal.get(Calendar.YEAR));
		System.out.print(" at ");
		if (this.cal.get(Calendar.HOUR_OF_DAY)<10) {
			System.out.print("0"+this.cal.get(Calendar.HOUR_OF_DAY));
		}
		else {System.out.print(this.cal.get(Calendar.HOUR_OF_DAY));}
		System.out.print(":");
		if(this.cal.get(Calendar.MINUTE)<10) {
			System.out.println("0"+this.cal.get(Calendar.MINUTE));
		}
		else {
			System.out.println(this.cal.get(Calendar.MINUTE));}
				
	}
	
	
	
}
